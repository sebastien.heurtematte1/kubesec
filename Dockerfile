ARG SCANNER_VERSION=2.12.0
ARG SCANNER_BIN_SHA256=d6fe7cc0fbbf3ec5ac72fa94c3d1db97081b025a1772e906b6a0e58be1cbf386

FROM alpine/helm:3.9.0 AS helm

FROM golang:1.17-alpine AS build
ARG SCANNER_VERSION
ARG SCANNER_BIN_SHA256
ENV CGO_ENABLED=0
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

# The "kubesec" user (from the kubesec/kubesec image) doesn't have permission to create a
# file in /etc/ssl/certs, this RUN command creates files that the
# analyzer can modify.
RUN touch /ca-certificates.crt

ADD https://github.com/controlplaneio/kubesec/releases/download/v${SCANNER_VERSION}/kubesec_linux_amd64.tar.gz /tmp/kubesec.tar.gz
RUN echo "$SCANNER_BIN_SHA256  /tmp/kubesec.tar.gz" | sha256sum -c
RUN tar -xf /tmp/kubesec.tar.gz
RUN mv kubesec /tmp/kubesec

FROM alpine:3.16.2
USER root
RUN apk update && apk add git apk-tools=2.12.9-r3 && apk upgrade

RUN addgroup -S kubesec && adduser -S kubesec -G kubesec
USER kubesec

ARG SCANNER_VERSION
ENV SCANNER_VERSION $SCANNER_VERSION

COPY --from=helm /usr/bin/helm /usr/bin/helm
ENV PATH="/home/app:${PATH}"
COPY --from=build /go/src/app/analyzer /
COPY --from=build /tmp/kubesec /bin/kubesec
COPY --from=build --chown=kubesec:kubesec /ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

ENTRYPOINT []
CMD ["/analyzer", "run"]
