package main

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/command"
	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2/plugin"

	log "github.com/sirupsen/logrus"
)

func main() {
	app := command.NewApp(metadata.AnalyzerDetails)

	app.Commands = command.NewCommands(command.Config{
		Match:        plugin.Match,
		Analyze:      analyze,
		Analyzer:     metadata.AnalyzerDetails,
		AnalyzeFlags: analyzeFlags(),
		Convert:      convert.Convert,
		Scanner:      metadata.ReportScanner,
		ScanType:     metadata.Type,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
