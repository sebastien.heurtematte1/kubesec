# Kubesec analyzer changelog

## v3.4.3
- Update Kubesec version 2.11.5 => 2.12.0 (!79)

## v3.4.2
- Automatically fetch Helm dependencies for Helm projects (!75)

## v3.4.1
- Fix bug causing partial findings to be reported for manifests with multiple objects (!77)

## v3.4.0
- upgrade `github.com/urfave/cli/v2` version [`v2.16.3` => [`v2.19.2`](https://github.com/urfave/cli/releases/tag/v2.19.2)] (!73)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.9.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.2)] (!73)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.1` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!73)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.13.0` => [`v3.15.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.15.2)] (!73)

## v3.3.2
- Decouple the analyzer image from Kubesec container image (!71)

## v3.3.1
- Update common to `v3.2.1` to fix gotestsum cmd (!72)

## v3.3.0
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` => [`v2.16.3`](https://github.com/urfave/cli/releases/tag/v2.16.3)] (!70)

## v3.2.1
- Fix golangci-lint `prealloc` linter errors (!67)

## v3.2.0
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!68)
- upgrade `github.com/stretchr/testify` version [`v1.7.0` => [`v1.8.0`](https://github.com/stretchr/testify/releases/tag/v1.8.0)] (!68)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!68)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.0` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!68)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!68)

## v3.1.0
- Upgrade core analyzer dependencies (!66)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.0.3
- Bump Kubesec to 2.11.5 (!65)
  - chore: update and simplify future updates [327](https://github.com/controlplaneio/kubesec/pull/327)

## v3.0.2
- Fix malformed manifests when helm outputs to stderr (!64)

## v3.0.1
- Bump helm to 3.9.0 (!63)

## v3.0.0
- Bump to 3.0.0 (!62)

## v2.17.0
- Update ruleset, report, and command modules to support ruleset overrides (!60)

## v2.16.2
- Update kubesec to [v2.11.4](https://github.com/controlplaneio/kubesec/releases/tag/v2.11.4) (!58)
    - fixup tag builds and avoid building anything other than release tags
    - update dependencies

## v2.16.1
- Update common to `v2.24.1` which fixes a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!57)

## v2.16.0
- feat: Analyze manifests concurrently (!55)

## v2.15.1
- chore: Update go to v1.17 (!56)

## v2.15.0
- Addressed all vulnerabilities reported by DS, SAST and container scanning (!53)
    - Fix: Use a vuln-free alpine docker base image
    - Fix: Upgrade and pin apk-tools to a vuln-free version
    - Fix: Addressed reported gosec issues

## v2.14.0
- Update report dependency in order to use the report schema version 14.0.0 (!47)

## v2.13.0
- Feat: updates `primary_identifier` value to kubesec rule ID (!46)

## v2.12.0
- Update kubesec to [v2.11.0](https://github.com/controlplaneio/kubesec/releases/tag/v2.11.0) (!45)
    - Move assets in the containers to make them easier to access
    - Fix changelog links
    - Add exit-code override

## v2.11.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!43)

## v2.11.0
- Update helm docker image to v3.4.2 (!42)

## v2.10.0
- Upgrade common to v2.22.0 (!41)
- Update urfave/cli to v2.3.0 (!41)

## v2.9.0
- Update kubesec to v2.7.2 (!40)
- Update logrus, cli golang dependencies to latest versions

## v2.8.0
- Update common and enable disablement of custom rulesets (!39)

## v2.7.2
- Fix bug which prevented writing `ADDITIONAL_CA_CERT_BUNDLE` value to `/etc/gitconfig` (!35)

## v2.7.1
- Update helm to [v3.3.1](https://github.com/helm/helm/releases/tag/v3.3.1) (!31)
- Update golang dependencies

## v2.7.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!30)

## v2.6.1
- Update kubesec to [v2.6.0](https://github.com/controlplaneio/kubesec/releases/tag/v2.6.0) (!27)
- Update golang to v1.15
- Update helm docker image

## v2.6.0
- Add scan object to report (!24)

## v2.5.1
- Update kubesec to [v2.5.0](https://github.com/controlplaneio/kubesec/releases/tag/v2.5.0) (!22)

## v2.5.0
- Switch to the MIT Expat license (!21)

## v2.4.0
- Update logging to be standardized across analyzers (!20)

## v2.3.0
- update helm to [3.2.4](https://github.com/helm/helm/releases/tag/v3.2.4) (!18 @phumberdroz)

## v2.2.4
- Update kubesec to [v2.4.0](https://github.com/controlplaneio/kubesec/releases/tag/v2.4.0) (!17)

## v2.2.3
- Add support for helm charts (!8 @agixid)

## v2.2.2
- Remove `location.dependency` from the generated SAST report (!16)

## v2.2.1
- Use Alpine as builder image (!13)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!10)

## v2.1.0
- Add support for custom CA certs (!7)

## v2.0.1
- Move away from use of CI_PROJECT_DIR variable

## v2.0.0
- Initial release
