module gitlab.com/gitlab-org/security-products/analyzers/kubesec/v2

go 1.15

require (
	github.com/Microsoft/go-winio v0.5.2 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20220711121315-1fde58898e96 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/imdario/mergo v0.3.13 // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51
	github.com/kevinburke/ssh_config v1.2.0 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.8.0
	github.com/urfave/cli/v2 v2.19.2
	gitlab.com/gitlab-org/security-products/analyzers/command v1.9.2
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.2
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.15.3
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/net v0.0.0-20220708220712-1185a9018129 // indirect
)
